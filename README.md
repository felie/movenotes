

# Why sidenotes ?

On the web pages (and maybe in books<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>), sidenotes are better. To scroll the page to read a footnote is awful. Sidenotes are one of the solutions to have several levels<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup> of texts.


# Where is the problem ?

In the code results of exports (in markdown, in org-mode, etc&#x2026;) the notes are all *after* the article.

It's not so easy to place them in the browser at the right (or left) of the note call.


# What are solutions ?


## 1.  Modify the export process.

Make the problem go away is always a solution&#x2026; That's the way the Shadoks<sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup> think.


## 2.  Modify the DOM<sup><a id="fnr.4" class="footref" href="#fn.4">4</a></sup> of the page.

It's the way of the very famous work of Gwen Branwen<sup><a id="fnr.5" class="footref" href="#fn.5">5</a></sup> for this page : [Radiance](https://www.gwern.net/docs/radiance/2002-scholz-radiance#sn1). A 950 lines js long<sup><a id="fnr.6" class="footref" href="#fn.6">6</a></sup>.


# Why move the notes ?

Modify the export is easy but a bad idea: it's a fork of the standard export in fact.

I move notes *in situ* in the org-mode DOM in 14 lines<sup><a id="fnr.7" class="footref" href="#fn.7">7</a></sup> of pure javascript<sup><a id="fnr.8" class="footref" href="#fn.8">8</a></sup> (not jquery at all).

See online the [the result](https://elie.org/movenotes/README.html)


# Howto

-   Simply put movenotes.css and movenotes.js in the directory where you are exporting in html
-   put the line

    #+HTML_HEAD: &lt;script type="text/javascript" src="movenotes.js"></script&gt;&lt;link rel="stylesheet" type="text/css" href="movenotes.css"/&gt;

in the header of your org page. 

-   export with C-c C-e h o and see<sup><a id="fnr.9" class="footref" href="#fn.9">9</a></sup>


# TODO

-   Put the comments at the left
-   DONE Note call in title give title notes&#x2026; see note about DOM here!
-   Adapt the code to markdown ?


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> In Bible or old editions in philosophy it's very common.

<sup><a id="fn.2" href="#fnr.2">2</a></sup> Other is *interpolation* for example.

<sup><a id="fn.3" href="#fnr.3">3</a></sup> Those who still not know Shadoks are lucky. See [The Shadoks](https://en.wikipedia.org/wiki/Les_Shadoks) and enjoy!

<sup><a id="fn.4" href="#fnr.4">4</a></sup> Document Object Model: the tree of the HTML page in which the javascript can operate.

<sup><a id="fn.5" href="#fnr.5">5</a></sup> See [a reddit post about it.](https://www.reddit.com/r/gwern/comments/b13p93/sidenotesjs_new_gwernnet_feature_for_turning/)

<sup><a id="fn.6" href="#fnr.6">6</a></sup> [sidenotes.js](https://www.gwern.net/static/js/sidenotes.js)

<sup><a id="fn.7" href="#fnr.7">7</a></sup> Two days for 14 lines I will remember!

<sup><a id="fn.8" href="#fnr.8">8</a></sup> See  [movenotes.js](./movenotes.js)

<sup><a id="fn.9" href="#fnr.9">9</a></sup> ![img](./cadeau.png)
