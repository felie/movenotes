//
// code for sidenotes for org-export in html
// put in your org header the line:
// #+HTML_HEAD <script type="text/javascript" src="movenotes.js"></script><link rel="stylesheet" type="text/css" href="movenotes.css" >
//
// François Elie <francois AT elie DOT org> 2020
//

function movenotes(){
    if (window.innerWidth<window.innerHeight){
	return;
    }    
    footnotes=document.getElementById("footnotes");
    //footnotes.style.display="none"; // on efface les notes de bas de pages
    notes = document.getElementsByClassName("footref"); // on prend un tableau des appels de notes
    for (var i = 0; i < notes.length; i++) { // pour chaque appel de note, on déplace la note à côté de l'appel (la feuille css fera le reste)
        movenote(notes[i]);
    }
    footnotes.innerHTML="";
}
function movenote(i){
    num = i.innerHTML; // on récupère le numéro de la note
    var valeur = document.createElement('div'); // on crée un div
    valeur.className="sidenote"; // de la classe sidenote
    valeur.innerHTML = '<sup><a id="fn.'+num+'" class="footnum" href="#fnr.'+num+'">'+num+'</sup></a> '+document.getElementById("fn."+num).parentElement.parentElement.children[1].children[0].innerHTML; // on récupère sa valeur :)
    i.parentElement.parentNode.insertBefore(valeur, i.parentElement.nextSibling); // on l'installe après (il n'y a pas d'insertAfter en javascript, alors...)
};
window.onload=movenotes;


